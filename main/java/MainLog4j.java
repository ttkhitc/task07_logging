import org.apache.logging.log4j.*;

public class MainLog4j {

	private static Logger logger1 = LogManager.getLogger(MainLog4j.class);

	public static void main(String[] args) {
		logger1.trace("This is a trace message\n");
		logger1.debug("This is a debug message\n");
		logger1.info("This is an info message\n");
		logger1.warn("This is a warn message\n");
		logger1.error("This is an error message\n");
		logger1.fatal("This is a fatal message\n");

		System.out.println("\n End of App");
	}

}
